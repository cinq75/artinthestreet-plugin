<?php

function register_custom_acf_fields_header_footer()
{
    if (function_exists('acf_add_local_field_group'))://first add the fields

acf_add_local_field_group(array(
    'key' => 'group_5ab8ade5f2cb1',
    'title' => 'Footer Code',
    'fields' => array(
        array(
            'key' => 'field_5ab8adec8ab7f',
            'label' => 'Footer Code',
            'name' => 'footer_code',
            'type' => 'textarea',
            'instructions' => 'Add code to be inserted in footer.',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'maxlength' => '',
            'rows' => '',
            'new_lines' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'acf-options-footer',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

    acf_add_local_field_group(array(
    'key' => 'group_5ab8adb84f6b0',
    'title' => 'Header Settings',
    'fields' => array(
        array(
            'key' => 'field_5ab8adc1dcf35',
            'label' => 'Header Code',
            'name' => 'header_code',
            'type' => 'textarea',
            'instructions' => 'Insert code to add to header.',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'maxlength' => '',
            'rows' => '',
            'new_lines' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'acf-options-header',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

    endif;
}

add_action('init', 'register_custom_acf_fields_header_footer');//call it...
// Add scripts to wp_head()
function child_theme_head_script()
{
    // Your PHP goes here

  the_field('header_code', 'option');
}


add_action('wp_head', 'child_theme_head_script');
// Add scripts to wp_footer()
function child_theme_footer_script()
{
    the_field('footer_code', 'option');
}
add_action('wp_footer', 'child_theme_footer_script');
