<?php
function wpb_load_fa()
{
    wp_enqueue_script('wpb-fa', 'https://use.fontawesome.com/releases/v5.7.2/js/all.js');
}

add_action('wp_enqueue_scripts', 'wpb_load_fa');
?>
